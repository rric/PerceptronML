# PerceptronML: demonstration of a simple machine learning algorithm

This repository contains some demonstrations of a very simple machine learning
algorithm: the Perceptron. This algorithm dates back to the late 1950s, when it
was first published by Frank Rosenblatt. It is able to find a straight line to
separate two sets of points, if such a line exists at all. Although the
Perceptron algorithm is simple and limited, it already shows some typical
features of machine learning; moreover, the math behind it is at high school
level.

These sketches are to be used in [Processing](https://processing.org/) Java 
mode:

+ `PerceptronDemo` is an interactive demonstration of the Perceptron algorithm.
    This sketch can be used offline; you can find an online version at
    <https://openprocessing.org/sketch/135826/>

+ `Cookies` captures live images from a camera, and detects foreground objects,
    such as various types of cookies, coins, crown caps, etc. Two features are
    computed per object: brightness and size. One can then train a Perceptron
    to differentiate between these types. The sketch shows both the live image,
    and a panel visualizing the Perceptron algorithm.

+ `CaptureSeries` is a utility to capture live images from a camera; these 
    images can then be used for an offline demo of `Cookies`.

### Literature

Rosenblatt, F. (1958). The perceptron: a probabilistic model for information 
storage and organization in the brain. _Psychological review, 65(6), 386_.
