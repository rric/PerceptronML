/* Captures and stores a series of images from camera
 * Copyright 2015 Johannes Kepler Universität Linz,
 *   Institut f. Wissensbasierte Mathematische Systeme
 * Copyright 2019, 2021, 2022 Roland Richter
 * Written for Processing 4 Java mode, released under GPLv3+
 */

import processing.video.*;

Capture video;


void setup()
{
    surface.setResizable(true);
    String[] cameras = Capture.list();

    if (cameras.length == 0) {
        println("There are no cameras available for capture.");
        exit();
    } else {
        println("There are", cameras.length, "cameras available for capture.");

        for (int i = 0; i < cameras.length; ++i) {
            println(i, ":", cameras[i]);
        }

        // Determine best camera device available, and open it.
        // For the 2021 demo, that is an "Doccamera" device of size
        // 640x480 with a frame rate of 30 fps. If this was not found, fall
        // back to another Doccamera device, or to the very first device.

        int bestCamera = 0;
        boolean found = false;

        for (int i = 0; !found && i < cameras.length; ++i) {
            if (match(cameras[i], "Doccamera") != null) {
                bestCamera = i;
                if (match(cameras[i], "size=640x480") != null
                        && match(cameras[i], "fps=30") != null) {
                    found = true;
                }
            }
        }

        print("Opening camera", bestCamera, ":", cameras[bestCamera], "...");

        video = new Capture(this, cameras[bestCamera]);
        video.start();
    }

    // Wait for first frame, and read it. Only then, width and height are
    // initialized, and are used to set width and height of the drawing panel.

    int sec = second();

    while (!video.available()) {
        if (sec != second()) {
            print(".");
            sec = second();
        }
    }

    video.read();

    println(" ready.");
    println("Capturing frames of size", video.width, "x", video.height);

    rectMode(CENTER);
}


String filename = "blank"; // coin, crowncap, or blank
int    filecount = 0;

void draw()
{
    surface.setSize(video.width, video.height);

    if (video.available()) {
        video.read();
    }

    background(#FFFFFF);
    image(video, 0, 0);

    if (keyPressed) {
        if (key == ENTER || key == ' ') {
            filecount += 1;
            String filepath = "data/" + filename + '-' + nf(filecount) + ".jpg";
            println("saving", filepath, "...");

            save(filepath);
        }
    }
}


/* --------------------------------------------------------------------
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
